'use strict';

bookApp.controller('BookListCtrl', function($scope, $http) {

    $http.get('data/book.json').success(function(data) {
        $scope.book = data;
    });
});

